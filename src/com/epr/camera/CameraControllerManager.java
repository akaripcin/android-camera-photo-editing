package com.epr.camera;

public abstract class CameraControllerManager {
	public abstract int getNumberOfCameras();
	abstract boolean isFrontFacing(int cameraId);
}
